const {sanitizeAddressName} = require("../src/index");

test("check addresses from at-w-1010.json", () => {
    const replacements = require("./replacements/at-w-1010.json");

    for (const [original, sanitized] of replacements) {
        expect(sanitizeAddressName(original)).toBe(sanitized);
    }
});

test("check addresses from at-w-1220.json", () => {
    const replacements = require("./replacements/at-w-1220.json");

    for (const [original, sanitized] of replacements) {
        expect(sanitizeAddressName(original)).toBe(sanitized);
    }
});

test("check addresses from at-w-general.json", () => {
    const replacements = require("./replacements/at-w-general.json");

    for (const [original, sanitized] of replacements) {
        expect(sanitizeAddressName(original)).toBe(sanitized);
    }
});
