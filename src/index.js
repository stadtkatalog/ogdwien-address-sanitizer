// StadtKatalog Address Sanitizer
// Copyright (C) 2018 Philipp Naderer-Puiu
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

"use strict";

/**
 * Sanitizes the given address name, e.g. removes uppercase sub-strings and fixes German Umlauts.
 * This functions is not optimized for performance, but for readability. Use it carefully!
 * @param name an address name
 * @returns {string} sanitized version of the given name
 */
exports.sanitizeAddressName = function(name) {
    // start with the lookbehind regex
    let replacedName = name.replace(/(?<!Objekt|Weg|Gebäude|Haus|Stiege|Steig|Block|Gruppe|OBJEKT|WEG|GEBÄUDE|HAUS|STIEGE|STEIG|BLOCK|GRUPPE) [A-Z] /, " ");

    // now the usual replacements
    replacedName = replacedName
        .replace(" GRST.NR. ", " Grundstück Nr. ")                  // u.a. Mühlhäufel GRST.NR. 341/7
        .replace("/GRP. ", "/Gruppe ")                              // u.a. Industriestraße 132A/GRP. F/Parzelle 5
        .replace(" GRUPPE ", " Gruppe ")                            // Kleine-Bucht-Straße 38 GRUPPE F Parzelle 13
        .replace(" GEG.", " gegenüber ")                            // u.a. Albertinaplatz GEG.D.OPER(STAENDE)
        .replace(" GEGENUEBER ", " gegenüber ")                     //
        .replace(" GEGUE.", " gegenüber ")                          //
        .replace(" GGUE.", " gegenüber ")                           // u.a. Alte Straße GGUE. 33
        .replace(/ NEB\.(\d)? ?/, " neben $1")                      // u.a. Kärntner Straße NEB. 37(KIRCHE)
        .replace(/ NEB /, " neben ")
        .replace(/ NEBEN /, " neben ")
        .replace(/ ECKE /, " Ecke ")
        .replace("/C WC", " WC")                                    // Stadtpark/C WC-ANLAGE
        .replace("GASTRO-KIOSK", "Gastro-Kiosk")                    // Franz-Josefs-Kai 6 GASTRO-KIOSK 1
        .replace(" VORKAI(LOKAL FLEX)", " Vorkai (Lokal Flex)")     // Franz-Josefs-Kai VORKAI(LOKAL FLEX)
        .replace(" SALZTORBR.(LOKAL)", " Salztorbrücke (Lokal)")    // Schanzelufer SALZTORBR.(LOKAL)
        .replace(" WC-ANLAGEN", " WC-Anlagen")
        .replace(" WC-ANLAGE", " WC-Anlage")                        // u.a. Irisgasse WC-ANLAGE
        .replace(" NEB. 1A(STAND)", " 1A (Stand)")                  // Universitätsring NEB. 1A(STAND)
        .replace(" GRABEN", " Graben")                              // Spiegelgasse ECKE GRABEN(STAND)
        .replace(" D.OPER", " der Oper")                            // u.a. Albertinaplatz GEG.D.OPER(STAENDE)
        .replace("WSTW,WC,KIOSK", "WSTW/WC/Kiosk")
        .replace("Bahnwächterhaus BAHNWÄCHTERHAUS", "Bahnwächterhaus")
        .replace(" FRIEDH.", " Friedhof")
        .replace("((FA. ULRICH)", " (Firma Ulrich)")

        .replace(/([0-9][A-Z])?( )?(\()?POLIZEIHUNDEABRICHTEPLATZ(\))?$/,              "$1 $3Polizeihundeabrichteplatz$4")
        .replace(/([0-9][A-Z])?( )?(\()?SCHIESSSTAND(\))?$/,              "$1 $3Schießstand$4")
        .replace(/([0-9][A-Z])?( )?(\()?PENSIONISTENWOHNHAUS(\))?$/,      "$1 $3Pensionistenwohnhaus$4")
        .replace(/([0-9][A-Z])?( )?(\()?TURNVEREIN(\))?$/,              "$1 $3Turnverein$4")
        .replace(/([0-9][A-Z])?( )?(\()?LEITBUNKER(\))?$/,              "$1 $3Leitbunker$4")                    //
        .replace(/([0-9][A-Z])?( )?(\()?GASDRUCKREGELSTATION(\))?$/,    "$1 $3Gasdruckregelstation$4")
        .replace(/([0-9][A-Z])?( )?(\()?GASDRUCKREGLERSTATION(\))?$/,   "$1 $3Gasdruckreglerstation$4")
        .replace(/([0-9][A-Z])?( )?(\()?BUNKER(\))?$/,                  "$1 $3Bunker$4")
        .replace(/([0-9][A-Z])?( )?(\()?CAFE-RESTAURANT(\))?$/,         "$1 $3Café-Restaurant$4")
        .replace(/([0-9][A-Z])?( )?(\()?EVANG. KIRCHE(\))?$/,           "$1 $3Evangelische Kirche$4")
        .replace(/([0-9][A-Z])?( )?(\()?VOTIVPARK-GARAGE(\))?$/,              "$1 $3Votivpark-Garage$4")
        .replace(/([0-9][A-Z])?( )?(\()?VOLKSHOCHSCHULE(\))?$/,              "$1 $3Volkshochschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?HOCHSCHULE(\))?$/,              "$1 $3Hochschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?KINDERKRANKENPFLEGESCHULE(\))?$/,              "$1 $3Kinderkrankenpflegeschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?KRANKENPFLEGESCHULE(\))?$/,              "$1 $3Krankenpflegeschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?WOHNBEZIRK(\))?$/,              "$1 $3Wohnbezirk$4")
        .replace(/([0-9][A-Z])?( )?(\()?RADIOSTATION(\))?$/,              "$1 $3Radiostation$4")
        .replace(/([0-9][A-Z])?( )?(\()?MINIGOLFPLATZ(\))?$/,              "$1 $3Minigolfplatz$4")
        .replace(/([0-9][A-Z])?( )?(\()?BAUERNHOF(\))?$/,              "$1 $3Bauernhof$4")
        .replace(/([0-9][A-Z])?( )?(\()?SENIORENWOHNHAUS(\))?$/,              "$1 $3Seniorenwohnhaus$4")
        .replace(/([0-9][A-Z])?( )?(\()?POLIZEIUEBUNGSPLATZ(\))?$/,              "$1 $3Polizeiübungsplatz$4")
        .replace(/([0-9][A-Z])?( )?(\()?RETTUNGSSTATION(\))?$/,              "$1 $3Rettungsstation$4")
        .replace(/([0-9][A-Z])?( )?(\()?GEG\.GASDRUCKREGLER(\))?$/,              "$1 $3gegenüber Gasdruckregler$4")

        .replace(/([0-9][A-Z])?( )?(\()?WC ANLAGE(\))?$/,              "$1 $3WC-Anlage$4")
        .replace(/([0-9][A-Z])?( )?(\()?KEBABSTAND(\))?$/,              "$1 $3Kebabstand$4")
        .replace(/([0-9][A-Z])?( )?(\()?MARONISTAND(\))?$/,              "$1 $3Maronistand$4")
        .replace(/([0-9][A-Z])?( )?(\()?TRAFIK(\))?$/,              "$1 $3Trafik$4")
        .replace(/([0-9][A-Z])?( )?(\()?MARKTSTAND(\))?$/,              "$1 $3Marktstand$4")
        .replace(/([0-9][A-Z])?( )?(\()?BERUFSSCHULE(\))?$/,              "$1 $3Berufsschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?TAGESZENTRUM(\))?$/,              "$1 $3Tageszentrum$4")
        .replace(/([0-9][A-Z])?( )?(\()?BUNKER-LOKAL(\))?$/,              "$1 $3Bunker-Lokal$4")
        .replace(/([0-9][A-Z])?( )?(\()?LOKAL(\))?$/,              "$1 $3Lokal$4")
        .replace(/([0-9][A-Z])?( )?(\()?VERKAUFSLOKALE(\))?$/,              "$1 $3Verkaufslokale$4")
        .replace(/([0-9][A-Z])?( )?(\()?GESCHAEFTSLOKALE(\))?$/,              "$1 $3Geschäftslokale$4")
        .replace(/([0-9][A-Z])?( )?(\()?TABAKKIOSK(\))?$/,              "$1 $3Tabakkiosk$4")
        .replace(/([0-9][A-Z])?( )?(\()?TRAFIKSTAND(\))?$/,              "$1 $3Trafikstand$4")
        .replace(/([0-9][A-Z])?( )?(\()?STAND\+KIOSK(\))?$/,              "$1 $3Stand+Kiosk$4")

        .replace(/([0-9][A-Z])?( )?(\()?KLOSTERSCHULE(\))?$/,           "$1 $3Klosterschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?U-BAHN ABSTELLHALLE(\))?$/,     "$1 $3U-Bahn Abstellhalle$4")           // 1220 -> Prandaugasse 11(U-BAHN ABSTELLHALLE)
        .replace(/([0-9][A-Z])?( )?(\()?BETRIEB(\))?$/,                 "$1 $3Betrieb$4")                       // 1010 -> Museumsquartier BETRIEBE
        .replace(/([0-9][A-Z])?( )?(\()?BETRIEBE(\))?$/,                "$1 $3Betriebe$4")                      // 1010 -> Museumsquartier BETRIEBE
        .replace(/([0-9][A-Z])?( )?(\()?RONACHER(\))?$/,                "$1 $3Ronacher$4")                      // 1010 -> Seilerstätte 9(RONACHER)
        .replace(/([0-9][A-Z])?( )?(\()?SCHULE U\.KINDERTAGESHEIM(\))?$/,"$1 $3Schule und Kindertagesheim$4")   // 1220 -> Prandaugasse 5(SCHULE U. KINDERTAGESHEIM)
        .replace(/([0-9][A-Z])?( )?(\()?SCHULE U\. KINDERTAGESHEIM(\))?$/,"$1 $3Schule und Kindertagesheim$4")  // 1220 -> Prandaugasse 5(SCHULE U. KINDERTAGESHEIM)
        .replace(/([0-9][A-Z])?( )?(\()?NEUE MITTELSCHULE(\))?$/,       "$1 $3Neue Mittelschule$4")             //
        .replace(/([0-9][A-Z])?( )?(\()?FAHRSCHULE(\))?$/,              "$1 $3Fahrschule$4")                    //
        .replace(/([0-9][A-Z])?( )?(\()?SEGELSCHULE(\))?$/,             "$1 $3Segelschule$4")                   //
        .replace(/([0-9][A-Z])?( )?(\()?MUSIKSCHULE(\))?$/,             "$1 $3Musikschule$4")                   //
        .replace(/([0-9][A-Z])?( )?(\()?HAUPTSCHULE(\))?$/,             "$1 $3Hauptschule$4")                   //
        .replace(/([0-9][A-Z])?( )?(\()?VOLKSSCHULE(\))?$/,             "$1 $3Volksschule$4")                   //
        .replace(/([0-9][A-Z])?( )?(\()?SCHULE(\))?$/,                  "$1 $3Schule$4")                        // 1010 -> Hegelgasse 14(SCHULE)
        .replace(/([0-9][A-Z])?( )?(\()?KIOSK(\))?$/,                   "$1 $3Kiosk$4")                         // 1010 -> Kärntner Straße VOR 6(KIOSK)
        .replace(/([0-9][A-Z])?( )?(\()?HAUSEINFAHRT-STAND(\))?$/,      "$1 $3Hauseinfahrt-Stand$4")            // 1010 -> Wollzeile 13(HAUSEINFAHRT-STAND)
        .replace(/([0-9][A-Z])?( )?(\()?(WUERSTEL|WÜRSTEL|WURSTEL)STAND(\))?$/,  "$1 $3Würstelstand$5")         // Innstraße Würstelstand
        .replace(/([0-9][A-Z])?( )?(\()?IMBISS-STAND(\))?$/,            "$1 $3Imbiss-Stand$4")                  //
        .replace(/([0-9][A-Z])?( )?(\()?STAND(\))?$/,                   "$1 $3Stand$4")                         // 1010 -> Kärntner Straße vor 42(STAND)
        .replace(/([0-9][A-Z])?( )?(\()?STAENDE(\))?$/,                 "$1 $3Stände$4")                        // 1010 -> Albertinaplatz gegenüber der Oper(STAENDE)
        .replace(/([0-9][A-Z])?( )?(\()?VOLKSGARTENRESTAURANT(\))?$/,   "$1 $3Volksgartenrestaurant$4")         // 1010 -> Volksgarten VOLKSGARTENRESTAURANT
        .replace(/([0-9][A-Z])?( )?(\()?RESTAURANT(\))?$/,              "$1 $3Restaurant$4")                    // 1010 -> Coburgbastei 4B (RESTAURANT)
        .replace(/([0-9][A-Z])?( )?(\()?PRUNKSTIEGE(\))?$/,             "$1 $3Prunkstiege$4")                   // 1010 -> Seilerstätte 3B (PRUNKSTIEGE)
        .replace(/([0-9][A-Z])?( )?(\()?PALAISSTIEGE(\))?$/,            "$1 $3Palaisstiege$4")                  // 1010 -> Seilerstätte 3A (PALAISSTIEGE)
        .replace(/([0-9][A-Z])?( )?(\()?KUNDENEINGANG(\))?$/,           "$1 $3Kundeneingang$4")                 // 1010 -> Seilerstätte 3A (PALAISSTIEGE)

        .replace(/([0-9][A-Z])?( )?(\()?RUNDSCHAUKEL(\))?$/,            "$1 $3Rundschaukel$4")                  //
        .replace(/([0-9][A-Z])?( )?(\()?TIEFPARKGARAGE(\))?$/,          "$1 $3Tiefparkgarage$4")                //

        .replace(/([0-9][A-Z])?( )?(\()?TIEFGARAGE(\))?$/,              "$1 $3Tiefgarage$4")                    // 1010 -> Franz-Josefs-Kai TIEFGARAGE
        .replace(/([0-9][A-Z])?( )?(\()?GARAGE(\))?$/,                  "$1 $3Garage$4")                        // 1010 -> Coburgbastei 5-7 (GARAGE)
        .replace(/([0-9][A-Z])?( )?(\()?HOTEL(\))?$/,                   "$1 $3Hotel$4")                         // 1010 -> Weihburggasse 32(HOTEL)
        .replace(/([0-9][A-Z])?( )?(\()?GASDRUCKREGELSTATION(\))?$/,    "$1 $3Gasdruckregelstation$4")          //
        .replace(/([0-9][A-Z])?( )?(\()?TRAFOSTATION(\))?$/,            "$1 $3Trafostation$4")                  // 1010 -> Alte Straße GGUE. 33 TRAFOSTATION
        .replace(/([0-9][A-Z])?( )?(\()?STATION(\))?$/,                 "$1 $3Station$4")                       // 1010 -> Stephansplatz U-Bahn (STATION)
        .replace(/([0-9][A-Z])?( )?(\()?PFARRE VOTIVKIRCHE(\))?$/,      "$1 $3Pfarre Votivkirche$4")            // Rooseveltplatz 8 (Pfarre Votivkirche)
        .replace(/([0-9][A-Z])?( )?(\()?VOTIVKIRCHE(\))?$/,             "$1 $3Votivkirche$4")                   // Rooseveltplatz 8 (Votivkirche)
        .replace(/([0-9][A-Z])?( )?(\()?KIRCHE(\))?$/,                  "$1 $3Kirche$4")                        // 1010 -> Kärntner Straße NEB. 37(KIRCHE)
        .replace(/([0-9][A-Z])?( )?(\()?PFARRE(\))?$/,                  "$1 $3Pfarre$4")                        // 1010 -> Minoritenplatz 2A(PFARRE)
        .replace(/([0-9][A-Z])?( )?(\()?RINGSTRASSENGALERIEN(\))?$/,    "$1 $3Ringstraßen-Galerien$4")          // 1010 -> Kärntner Ring 11-13(RINGSTRASSENGALERIEN)
        .replace(/([0-9][A-Z])?( )?(\()?HAUSEINFAHRT-STAND(\))?$/,      "$1 $3Hauseinfahrt-Stand$4")            // 1010 -> Wollzeile 13(HAUSEINFAHRT-STAND)
        .replace(/([0-9][A-Z])?( )?(\()?HOTEL BRISTOL(\))?$/,           "$1 $3Hotel Bristol$4")                 // 1010 -> Kärntner Ring 1(HOTEL BRISTOL)
        .replace(/([0-9][A-Z])?( )?(\()?HOTEL AUSTRIA(\))?$/,           "$1 $3Hotel Austria$4")                 // 1010 -> Wolfengasse 3(HOTEL AUSTRIA)
        .replace(/([0-9][A-Z])?( )?(\()?KASEMATTENEINGANG(\))?$/,       "$1 $3Kasematteneingang$4")             // 1010 -> Coburgbastei 4C (KASEMATTENEINGANG)
        .replace(/([0-9][A-Z])?( )?(\()?GASDRUCKREGLER(\))?$/,          "$1 $3Gasdruckregler$4")                // 1010 -> Franz-Josefs-Kai gegenüber 47(GASDRUCKREGLER)
        .replace(/([0-9][A-Z])?( )?(\()?WOHNGEBÄUDE(\))?$/,             "$1 $3Wohngebäude$4")                   // 1010 -> Eßlinger Hauptstraße 50 WOHNGEBÄUDE
        .replace(/([0-9][A-Z])?( )?(\()?SPORTZENTRUM(\))?$/,            "$1 $3Sportzentrum$4")                  // 1220 -> Eiswerkstraße 20 SPORTZENTRUM
        .replace(/([0-9][A-Z])?( )?(\()?SPORTPLATZ-SCHLOSSWIESE(\))?$/, "$1 $3Sportplatz Schloßwiese$4")        //
        .replace(/([0-9][A-Z])?( )?(\()?SPORTPLATZ(\))?$/,              "$1 $3Sportplatz$4")                    // 1220 -> Natorpgasse 2(SPORTPLATZ)
        .replace(/([0-9][A-Z])?( )?(\()?STUDENTENHEIM(\))?$/,           "$1 $3Studentenheim$4")                 // 1220 -> Josef-Baumann-Gasse 8A(STUDENTENHEIM)
        .replace(/([0-9][A-Z])?( )?(\()?GASDRUCKR\.(\))?$/,             "$1 $3Gasdruckregler$4")                // 1220 -> Groß-Enzersdorfer Straße HABERLANDTG.(GASDRUCKR.)
        .replace(/([0-9][A-Z])?( )?(\()?AMTSHAUS(\))?$/,                "$1 $3Amtshaus$4")                      // 1220 -> Schrödingerplatz 1 AMTSHAUS
        .replace(/([0-9][A-Z])?( )?(\()?NEBENGEBAEUDE(\))?$/,           "$1 $3Nebengebäude$4")                  // 1220 -> Zehdengasse 37 NEBENGEBAEUDE
        .replace(/([0-9][A-Z])?( )?(\()?NEBENGEBÄUDE(\))?$/,            "$1 $3Nebengebäude$4")                  // 1220 -> An der oberen Alten Donau 163 NEBENGEBÄUDE
        .replace(/([0-9][A-Z])?( )?(\()?WOHNGEBAEUDE(\))?$/,            "$1 $3Wohngebäude$4")                   // 1220 -> Korsenweg 42 WOHNGEBAEUDE
        .replace(/([0-9][A-Z])?( )?(\()?KOMPOSTWERK-LOBAU(\))?$/,       "$1 $3Kompostwerk Lobau$4")             // 1220 -> Lobgrundstraße KOMPOSTWERK-LOBAU
        .replace(/([0-9][A-Z])?( )?(\()?TRABRENNVEREIN(\))?$/,          "$1 $3Trabrennverein$4")                //
        .replace(/([0-9][A-Z])?( )?(\()?SPORTVEREIN(\))?$/,             "$1 $3Sportverein$4")                   //
        .replace(/([0-9][A-Z])?( )?(\()?VEREIN(\))?$/,                  "$1 $3Verein$4")                        // 1220 -> KLG Franz Siller VEREIN
        .replace(/([0-9][A-Z])?( )?(\()?SK\.HANDELSMINISTERIUM(\))?$/,  "$1 $3Sportklub Handelsministerium$4")  // 1220 -> Wielandweg 25(SK.HANDELSMINISTERIUM)
        .replace(/([0-9][A-Z])?( )?(\()?VERKEHRSKINDERGARTEN(\))?$/,    "$1 $3Verkehrskindergarten$4")          //
        .replace(/([0-9][A-Z])?( )?(\()?KINDERGARTEN(\))?$/,            "$1 $3Kindergarten$4")                  // 1220 -> Schödlbergergasse 7(KINDERGARTEN)
        .replace(/([0-9][A-Z])?( )?(\()?LAGERPLATZ(\))?$/,              "$1 $3Lagerplatz$4")                    // 1220 -> Schrickgasse 27(LAGERPLATZ)

        .replace(/([0-9][A-Z])?( )?(\()?POLIZEIHUNDEABT.(\))?$/,        "$1 $3Polizeihundeabteilung$4")         //
        .replace(/([0-9][A-Z])?( )?(\()?FW-BOOTSHAUS(\))?$/,            "$1 $3Feuerwehr-Bootshaus$4")           //
        .replace(/([0-9][A-Z])?( )?(\()?PFLANZENSCHUTZANSTALT(\))?$/,   "$1 $3Pflanzenschutzanstalt$4")         //
        .replace(/([0-9][A-Z])?( )?(\()?WIRTSCHAFTSGEBÄUDE(\))?$/,      "$1 $3Wirtschaftsgebäude$4")            //
        .replace(/([0-9][A-Z])?( )?(\()?GLASHAUS(\))?$/,                "$1 $3Glashaus$4")                      //
        .replace(/([0-9][A-Z])?( )?(\()?LAGERSCHUPPEN(\))?$/,           "$1 $3Lagerschuppen$4")                 //
        .replace(/([0-9][A-Z])?( )?(\()?PFLANZENHAUS(\))?$/,            "$1 $3Pflanzenhaus$4")                  //
        .replace(/([0-9][A-Z])?( )?(\()?UMKLEIDEHAUS(\))?$/,            "$1 $3Umkleidehaus$4")                  //
        .replace(/([0-9][A-Z])?( )?(\()?AMTSGEBÄUDE(\))?$/,             "$1 $3Amtsgebäude$4")                   //

        .replace(/([0-9][A-Z])?( )?(\()?WERKSTÄTTENGEBÄUDE(\))?$/,      "$1 $3Werkstättengebäude$4")            //
        .replace(/([0-9][A-Z])?( )?(\()?BETRIEBSGEBÄUDE(\))?$/,         "$1 $3Betriebsgebäude$4")               //
        .replace(/([0-9][A-Z])?( )?(\()?PORTALGEBÄUDE(\))?$/,           "$1 $3Portalgebäude$4")                 //
        .replace(/([0-9][A-Z])?( )?(\()?WÄSCHERSTÖCKL(\))?$/,           "$1 $3Wäscherstöckl$4")                 //
        .replace(/([0-9][A-Z])?( )?(\()?JOSEFSSTÖCKL(\))?$/,            "$1 $3Josefsstöckl$4")                  //
        .replace(/([0-9][A-Z])?( )?(\()?INSPEKTIONSSTÖCKL(\))?$/,       "$1 $3Inspektionsstöckl$4")             //

        .replace(/([0-9][A-Z])?( )?(\()HAUS(\))?$/,                     "$1 $3Haus$4")                          // 1220 -> Stallarngasse 122(HAUS)
        .replace(/([0-9][A-Z])?( )?(\()?LAGERPL\.(\))?$/,               "$1 $3Lagerplatz$4")                    // 1220 -> Azaleengasse ECKE OLEANDERG.(LAGERPL.)
        .replace(/([0-9][A-Z])?( )?(\()?HALLENBAD(\))?$/,               "$1 $3Hallenbad$4")                     // 1220 -> Portnergasse 38 HALLENBAD
        .replace(/([0-9][A-Z])?( )?(\()?MÜLLRAUM(\))?$/,                "$1 $3Müllraum$4")                      // 1220 -> Doningasse 18 MÜLLRAUM
        .replace(/([0-9][A-Z])?( )?(\()?WOHNHAUS(\))?$/,                "$1 $3Wohnhaus$4")                      // 1220 -> Zehdengasse 37 WOHNHAUS
        .replace(/([0-9][A-Z])?( )?(\()?HOSPIZ(\))?$/,                  "$1 $3Hospiz$4")                        // 1220 -> Sinagasse 58(HOSPIZ)
        .replace(/([0-9][A-Z])?( )?(\()?RUDERKLUB(\))?$/,               "$1 $3Ruderklub$4")                     // 1220 -> Florian-Berndl-Gasse 10 RUDERKLUB
        .replace(/([0-9][A-Z])?( )?(\()?PETER MAX(\))?$/,               "$1 $3Peter MAX$4")                     // 1220 -> Gewerbeparkstraße 7(PETER MAX)
        .replace(/([0-9][A-Z])?( )?(\()?BOOTSVERLEIH(\))?$/,            "$1 $3Bootsverleih$4")                  // 1220 -> Schnitterweg 15 BOOTSVERLEIH
        .replace(/([0-9][A-Z])?( )?(\()?NEU(\))?$/,                     "$1 $3Neu$4")                           // 1220 -> Rohrweihenweg 34(NEU)
        .replace(/([0-9][A-Z])?( )?(\()?WEINTANKSTELLE(\))?$/,          "$1 $3Weintankstelle$4")                //
        .replace(/([0-9][A-Z])?( )?(\()?AWI-TANKSTELLE(\))?$/,          "$1 $3AWI-Tankstelle$4")                //
        .replace(/([0-9][A-Z])?( )?(\()?AVANTI-TANKSTELLE(\))?$/,       "$1 $3AVANTI-Tankstelle$4")             //
        .replace(/([0-9][A-Z])?( )?(\()?BP-TANKSTELLE(\))?$/,           "$1 $3BP-Tankstelle$4")                 //
        .replace(/([0-9][A-Z])?( )?(\()?OMV-TANKSTELLE(\))?$/,          "$1 $3OMV-Tankstelle$4")                //
        .replace(/([0-9][A-Z])?( )?(\()?SHELL-TANKSTELLE(\))?$/,        "$1 $3Shell-Tankstelle$4")              //
        .replace(/([0-9][A-Z])?( )?(\()?TANKSTELLE(\))?$/,              "$1 $3Tankstelle$4")                    // 1220 -> Quadenstraße gegenüber 3(TANKSTELLE)
        .replace(/([0-9][A-Z])?( )?(\()?WC-ANLAGE(\))?$/,               "$1 $3WC-Anlage$4")                     // 1220 -> Schüttauplatz 2 gegenüber 2 (WC-ANLAGE)
        .replace(/([0-9][A-Z])?( )?(\()?PROBLEMSTOFFSAMMELST\.(\))?$/,  "$1 $3Problemstoffsammelstelle$4")      // 1220 -> Schüttauplatz 2 gegenüber 2 (PROBLEMSTOFFSAMMELST.)
        .replace(/([0-9][A-Z])?( )?(\()?KLOSTER(\))?$/,                 "$1 $3Kloster$4")                       // 1220 -> Schüttaustraße 41-43(KLOSTER)
        .replace(/([0-9][A-Z])?( )?(\()?ASPERNER FRIEDHOF(\))?$/,       "$1 $3Asperner Friedhof$4")             // 1220 -> Langobardenstraße 180(ASPERNER FRIEDHOF)
        .replace(/([0-9][A-Z])?( )?(\()?BESCHUSSAMT(\))?$/,             "$1 $3Beschussamt$4")                   // 1220 -> Langobardenstraße 180(ASPERNER FRIEDHOF)
        .replace(/([0-9][A-Z])?( )?(\()?EISSPORTHALLE(\))?$/,           "$1 $3Eissporthalle$4")                 //
        .replace(/([0-9][A-Z])?( )?(\()?SPORTHALLE(\))?$/,              "$1 $3Sporthalle$4")                    // 1220 -> Lieblgasse 4A SPORTHALLE
        .replace(/([0-9][A-Z])?( )?(\()?VEREINSHEIM(\))?$/,             "$1 $3Vereinsheim$4")                   // 1220 -> Dampfschiffhaufen 1 VEREINSHEIM
        .replace(/([0-9][A-Z])?( )?(\()?PUMPWERK(\))?$/,                "$1 $3Pumpwerk$4")                      // 1220 -> Wagramer Straße 300 PUMPWERK
        .replace(/([0-9][A-Z])?( )?(\()?KINDERTAGESHEIM(\))?$/,         "$1 $3Kindertagesheim$4")               // 1220 -> Doningasse 16 KINDERTAGESHEIM
        .replace(/([0-9][A-Z])?( )?(\()?DIALYSEZENTRUM(\))?$/,          "$1 $3Dialysezentrum$4")                // 1220 -> Kapellenweg 37 DIALYSEZENTRUM
        .replace(/([0-9][A-Z])?( )?(\()?STUDENTENWOHNHEIM(\))?$/,       "$1 $3Studentenwohnheim$4")             // 1220 -> Dückegasse 3/2 STUDENTENWOHNHEIM
        .replace(/([0-9][A-Z])?( )?(\()?BUEROHAUS(\))?$/,               "$1 $3Bürohaus$4")                      // 1220 -> Hermann-Gebauer-Straße 5 (BUEROHAUS)
        .replace(/([0-9][A-Z])?( )?(\()?GERÄTESCHUPPEN(\))?$/,          "$1 $3Geräteschuppen$4")                // 1220 -> Hermann-Gebauer-Straße 5 (GERÄTESCHUPPEN)
        .replace(/([0-9][A-Z])?( )?(\()?LAGERHALLE(\))?$/,              "$1 $3Lagerhalle$4")                    // 1220 -> Ambrosigasse 23 LAGERHALLE
        .replace(/([0-9][A-Z])?( )?(\()?DONAUPLEX(\))?$/,               "$1 $3Donau Plex$4")                    // 1220 -> Wagramer Straße 79(DONAUPLEX)
        .replace(/([0-9][A-Z])?( )?(\()?TENNISCLUB(\))?$/,              "$1 $3Tennisclub$4")                    // 1220 -> Biberhaufenweg 18A TENNISCLUB
        .replace(/([0-9][A-Z])?( )?(\()?LADENZEILE(\))?$/,              "$1 $3Ladenzeile$4")                    // 1220 -> Arakawastraße 2(LADENZEILE)
        .replace(/([0-9][A-Z])?( )?(\()?INSEL INFO(\))?$/,              "$1 $3Insel-Info$4")                    // 1220 -> Reichsbrücke 215 INSEL INFO
        .replace(/([0-9][A-Z])?( )?(\()?OBJEKT (\d+)(\))?$/,            "$1 $3Objekt $4$5")                     // 1220 -> Rehlackenweg 37(OBJEKT 1)
        .replace(/([0-9][A-Z])?( )?(\()?HAUS (\d+[A-Z])(\))?$/,         "$1 $3Haus $4$5")                       // 1220 -> Kalmusweg 53(HAUS 74B)
        .replace(/([0-9][A-Z])?( )?(\()?HAUS ([A-Z])(\))?$/,            "$1 $3Haus $4$5")                       // 1220 -> Memlinggasse 27(HAUS B)
        .replace(/([0-9][A-Z])?( )?(\()?HAUS (\d+\/\d+)(\))?$/,         "$1 $3Haus $4$5")                       // 1220 -> Biberhaufenweg 71(HAUS 7/8)
        .replace(/([0-9][A-Z])?( )?(\()?BLOCK ([A-Z]\/\d+)(\))?$/,      "$1 $3Block $4$5")                      // 1220 -> Hammerfestweg 2(BLOCK B/1)
        .replace(/([0-9][A-Z])?( )?(\()?BLOCK ([A-Z])(\))?$/,           "$1 $3Block $4$5")                      // 1220 -> Hammerfestweg 2(BLOCK F)
        .replace(/([0-9][A-Z])?( )?(\()?BLOCK (\d+)(\))?$/,             "$1 $3Block $4$5")                      // 1220 -> Kanalstraße 62-68(BLOCK 12)
        .replace(/ \(BLOCK ([A-Z]+[0-9]*)(\/ Haus .+)\)/,                    " (Block $1 $2)")                      // 1220 -> Lannesstraße 52 (Block C/ Haus 7)
        .replace(/\(BLOCK ([A-Z]+[0-9]*)\/HAUS (.+)\)/,                      " (Block $1/Haus $2)")                 // 1220 -> Lannesstraße 52 (Block C/ Haus 7)
        .replace(/([0-9][A-Z])?( )?(\()?RUDERVEREIN FRIESEN(\))?$/,                	"$1 $3Ruderverein Friesen$4")
        .replace(/([0-9][A-Z])?( )?(\()?BEZIRKSGERICHT(\))?$/,                		"$1 $3Bezirksgericht$4")
        .replace(/([0-9][A-Z])?( )?(\()?FORSTHAUS(\))?$/,                			"$1 $3Forsthaus$4")
        .replace(/([0-9][A-Z])?( )?(\()?RUDDA(\))?$/,                				"$1 $3Rudda$4")
        .replace(/([0-9][A-Z])?( )?(\()?BANK-AUSTRIA(\))?$/,                		"$1 $3Bank Austria$4")
        .replace(/([0-9][A-Z])?( )?(\()?KESSELZENTRALE(\))?$/,                		"$1 $3Kesselzentrale$4")
        .replace(/([0-9][A-Z])?( )?(\()?KINDERTAGESHEIM(\))?$/,                		"$1 $3Kindertagesheim$4")
        .replace(/([0-9][A-Z])?( )?(\()?STRANDBAD STADLAU(\))?$/,               	"$1 $3Strandbad Stadlau$4")
        .replace(/([0-9][A-Z])?( )?(\()?EKAZENT-STADLAU NORD(\))?$/,                "$1 $3EKAZENT-Stadlau Nord$4")
        .replace(/([0-9][A-Z])?( )?(\()?RESTAURANT ZWERNEMANN(\))?$/,               "$1 $3Restaurant Zwernemann$4")
        .replace(/([0-9][A-Z])?( )?(\()?VEREINSHAUS(\))?$/,                			"$1 $3Vereinshaus$4")
        .replace(/([0-9][A-Z])?( )?(\()?COPA CAGRANA(\))?$/,             			"$1 $3Copa Cagrana$4")
        .replace(/([0-9][A-Z])?( )?(\()?FLOHMARKT(\))?$/,                			"$1 $3Flohmarkt$4")
        .replace(/([0-9][A-Z])?( )?(\()?RUDERKLUB(\))?$/,                			"$1 $3Ruderklub$4")
        .replace(/([0-9][A-Z])?( )?(\()?IMBISS-STAND(\))?$/,             			"$1 $3Imbiss-Stand$4")
        .replace(/([0-9][A-Z])?( )?(\()?TRANSPORTBETON(\))?$/,           			"$1 $3Transportbeton$4")
        .replace(/([0-9][A-Z])?( )?(\()?POSTAMT(\))?$/,                  			"$1 $3Postamt$4")
        .replace(/([0-9][A-Z])?( )?(\()?LAGER(\))?$/,                	 			"$1 $3Lager$4")
        .replace(/([0-9][A-Z])?( )?(\()?SEGELSCHULE(\))?$/,              			"$1 $3Segelschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?HAUPTSCHULE(\))?$/,              			"$1 $3Hauptschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?NEUE MITTELSCHULE(\))?$/,        			"$1 $3Neue Mittelschule$4")
        .replace(/([0-9][A-Z])?( )?(\()?RENAULT(\))?$/,                	 			"$1 $3Renault$4")
        .replace(/([0-9][A-Z])?( )?(\()?BAECKEREI FELBER(\))?$/,         			"$1 $3Baeckerei Felber$4")
        .replace(/([0-9][A-Z])?( )?(\()?MOBILKLASSEN(\))?$/,             			"$1 $3Mobilklassen$4")
        .replace(/([0-9][A-Z])?( )?(\()?FRIEDHOF STADLAU(\))?$/,         			"$1 $3Friedhof Stadlau$4")
        .replace(/([0-9][A-Z])?( )?(\()?RIDI-LEUCHTEN(\))?$/,            			"$1 $3Ridi-Leuchten$4")
        .replace(/([0-9][A-Z])?( )?(\()?AVANTI-TANKSTELLE(\))?$/,        			"$1 $AVANTI-Tankstelle$4")
        .replace(/([0-9][A-Z])?( )?(\()?GRUNDWASSERWERK(\))?$/,                     "$1 $3Grundwasserwerk$4")
        .replace(/([0-9][A-Z])?( )?(\()?WASSERWERK(\))?$/,               			"$1 $3Wasserwerk$4")
        .replace(/([0-9][A-Z])?( )?(\()?SHELL(\))?$/,                	 			"$1 $3Shell$4")
        .replace(/([0-9][A-Z])?( )?(\()?MUELLVERWERTUNG\+MISTPLATZ(\))?$/,			"$1 $3Müllverwertung & Mistplatz$4")
        .replace(/([0-9][A-Z])?( )?(\()?FINANZAMT(\))?$/,                			"$1 $3Finanzamt$4")
        .replace(/([0-9][A-Z])?( )?(\()?BUFFET(\))?$/,                	 			"$1 $3Buffet$4")
        .replace(/([0-9][A-Z])?( )?(\()?BP-TANKSTELLE(\))?$/,            			"$1 $3BP-Tankstelle$4")
        .replace(/([0-9][A-Z])?( )?(\()?EISENBAHNERHEIM(\))?$/,          			"$1 $3Eisenbahnerheim$4")
        .replace(/([0-9][A-Z])?( )?(\()?FISCHERHUETTE(\))?$/,            			"$1 $3Fischerhütte$4")
        .replace(/([0-9][A-Z])?( )?(\()?EWSC-DONAU(\))?$/,               			"$1 $3EWSC Donau$4")
        .replace(/([0-9][A-Z])?( )?(\()?OMV(\))?$/,               			        "$1 $3OMV$4")
        .replace(/([0-9][A-Z])?( )?(\()?ÖBB(\))?$/,               			        "$1 $3ÖBB$4")
        .replace(/([0-9][A-Z])?( )?(\()?IM HOF(\))?$/,               			    "$1 $3im Hof$4")
        .replace(/([0-9][A-Z])?( )?(\()?MISTPLATZ D. MA 48(\))?$/,               	"$1 $3Mistplatz der MA 48$4")
        .replace(/([0-9][A-Z])?( )?(\()?EKAZENT(\))?$/,                			    "$1 $3EKAZENT$4")
        .replace(/([0-9][A-Z])?( )?(\()?MONDO(\))?$/,                			    "$1 $3MONDO$4")
        .replace(/([0-9][A-Z])?( )?(\()?ADEG(\))?$/,                			    "$1 $3ADEG$4")
        .replace(/([0-9][A-Z])?( )?(\()?TOYS 'R' US(\))?$/,                			"$1 $3Toys “R” Us$4")
        .replace(/([0-9][A-Z])?( )?(\()?GENDARM.SCHIESSTAETTE(\))?$/,               "$1 $3Gendamerie Schießstätte$4")
        .replace(/([0-9][A-Z])?( )?(\()?POLIZEISCHIESSTAETTE(\))?$/,                "$1 $3Polizeischießstätte$4")
        .replace(/([0-9][A-Z])?( )?(\()?POLIZEI SCHIESSTAETTE(\))?$/,               "$1 $3Polizeischießstätte$4")
        .replace(/([0-9][A-Z])?( )?(\()?WVB(\))?$/,                                 "$1 $3WVB$4")
        .replace(/([0-9][A-Z])?( )?(\()?FUSSGAENGERPASSAGE(\))?$/,                  "$1 $3Fußgängerpassage$4")


        .replace(/([a-zäöüß])(\()?STAND(\))?$/,       " $2Stand$3")           // Spiegelgasse Ecke Graben(STAND)
        .replace(/([a-zäöüß])(\()?STAENDE(\))?$/,     " $2Stände$3")          // Albertinaplatz gegenüber der Oper(STAENDE)


        .replace(" ERZBISCHOEFLICHES PALAIS", " Erzbischöfliche Palais")                    // Wollzeile 2A
        .replace(" RINGSTRASSENGALERIEN", " Ringstraßen-Galerien")                          // Akademiestraße 4 RINGSTRASSENGALERIEN
        .replace(" BIERGARTL", " Biergartl")                                                // Stadtpark BIERGARTL
        .replace(" HOTEL ASTORIA", " Hotel Astoria")                                        // Kärntner Straße 32 HOTEL ASTORIA
        .replace(" SCHULE", " Schule")                                                      // Stubenbastei 6-8 SCHULE
        .replace(" STUDENTENHEIM", " Studentenheim")                                        // Annagasse 9 STUDENTENHEIM
        .replace(" KLOSTER", " Kloster")                                                    // Weihburggasse 19 KLOSTER
        .replace(" UNIVERSITÄT", " Universität")                                            // u.a. Universitätsring 1 UNIVERSITÄT
        .replace(" VERWALTUNGSGERICHTSHOF", " Verwaltungsgerichtshof")                      // Jordangasse 2 VERWALTUNGSGERICHTSHOF
        .replace(/([0-9][A-Z]?)\(EXPEDIT WR.LOKALBAHNEN\)/, "$1 (Expedit Wiener Lokalbahnen)")
        .replace(/([0-9][A-Z]?)\(KINDERTAGESHEIM\)/, "$1 (Kindertagesheim)")                // Rudolfsplatz 5B(KINDERTAGESHEIM)
        .replace(/ VOR ([1-9])/, " vor $1")                                                 // u.a. Krugerstraße VOR 2 STAND
        .replace(" VOR STATION ", " vor Station ")                                          // u.a. U-Bahn-Station Kaisermühlen VOR STATION (Stand)
        .replace(" VOR OPER", " vor OPER")
        .replace("Burgtheater B BURGTHEATER", "Burgtheater")
        .replace(/( )?BURGTHEATER/, "$1Burgtheater")                // Universitätsring 2 BURGTHEATER
        .replace(/( )?KURSALON/, "$1Kursalon")                      // Johannesgasse 33 KURSALON
        .replace("Graben W ", "Graben ")
        .replace(" THESEUSTEMPEL", " Theseustempel")                // Volksgarten
        .replace(" GAERTNERHAUS", " Gärtnerhaus")                   // Volksgarten
        .replace(" MEIEREI", " Meierei")                            // Volksgarten
        .replace(" TANKSTELLE", " Tankstelle")                      // Volksgarten
        .replace(" VOLKSGARTENRESTAURANT", " Volksgartenrestaurant")// Volksgarten
        .replace("Volksgarten DANCING", "Volksgarten 30 Dancing")   // Volksgarten
        .replace(" STEPHANSDOM", " Stephansdom")                    // Stephansplatz 1 STEPHANSDOM
        .replace(" POLIZEI", " Polizei")                            // Kärntnertorpassage POLIZEI
        .replace(" PALAIS COBURG", " Palais Coburg")                // Coburgbastei 4 PALAIS COBURG
        .replace(" FAHRRADVERLEIH", " Fahrradverleih")              // Salztorbrücke FAHRRADVERLEIH
        .replace(" TIGRA HOTEL GARNI", " Tigra Hotel Grani")        // Tiefer Graben 14 TIGRA HOTEL GARNI
        .replace(" WEINGARTL", " Weingartl")                        // Stadtpark WEINGARTL
        .replace(/([0-1])\(HOTEL AUSTRIA\)$/, "$1 (Hotel Austria)") // Wolfengasse 3(HOTEL AUSTRIA)
        .replace(" Gaertnerunterkunft", " Gärtnerunterkunft")       // Rathauspark Gaertnerunterkunft

        // 1220
        .replace(/( )?(\()?HAUS (\d+)(\))?$/, " $2Haus $3$4")       // Gartenheimstraße 21(HAUS 21)
        .replace(/\/PARZ. (\d+)/, "/Parzelle $1")                   // Reglergasse 1A/PARZ. 100
        .replace(/ PARZ. (\d+)/, " Parzelle $1")                    // Naufahrtweg PARZ. 51
        .replace(/ Parz\. (\d+)/, " Parzelle $1")                   // KLG Paischer-Wasser Parz. 20
        .replace(" HABERLANDTG.", " Haberlandtgasse")               // Groß-Enzersdorfer Straße HABERLANDTG.(GASDRUCKR.)
        .replace(/^U-Bahn Station /, "U-Bahn-Station ")             // U-Bahn Station Aspernstraße
        .replace(/^Gewerbeparkstraße 19 19 SHOE FACTORY$/, "Gewerbeparkstraße 19 Shoe Factory")
        .replace(/^Breitenleer Friedhof \(FRIEDHOF BREITENLEE\)$/, "Breitenleer Friedhof")
        .replace(/ OLEANDERG\./, " Oleandergasse ")
        .replace(/([0-9])\(KIK\/FORSTINGER\/FIRSTFITNESS\)$/, "$1 (KiK/Forstinger/First Fitness)") // Gewerbeparkstraße 3(KIK/FORSTINGER/FIRSTFITNESS)
        .replace(/ FISCHERBOOT /, " Fischerboot ")                   // Donaustrom FISCHERBOOT 37A
        .replace(/ UNO-CITY$/, " UNO-City")                   // Wagramer Straße 5 UNO-CITY
        .replace(/15 15 DÄNISCHES BETTEN Lager$/, " 15 Dänisches Bettenlager")                   // Gewerbeparkstraße 15 15 DÄNISCHES BETTENLAGER
        .replace(/ 17 17 DE WITT,SUN COMPANY$/, " 17 De Witt, Sun Company")                   // Gewerbeparkstraße 17 17 DE WITT,SUN COMPANY
        .replace(" GR.BUCHT-STR. GASDRUCKREGL.", " Große-Bucht-Straße Gasdruckregler")      // Donaustadtstraße 482 GR.BUCHT-STR. GASDRUCKREGL.
        .replace(" KIOSK neben AUTOBUSBAHNHOF", " Kiosk neben Autobusbahnhof")      // Vernholzgasse KIOSK NEBEN AUTOBUSBAHNHOF

        .replace(" IM HOF ", " im Hof ")
        .replace(/ LEINER$/, " Leiner")                             // Rautenweg 55 LEINER
        .replace(/ HOFER/, " Hofer")
        .replace(/([^ ])\(BILLA\)/, "$1 (BILLA)")                   // Silenegasse 2(BILLA)
        .replace(/([^ ])\(SPAR\)/, "$1 (SPAR)")                     // Biberhaufenweg 117(SPAR)
        .replace(" NEUE DONAU ", " Neue Donau ")                    // Raffineriestraße NEUE DONAU
        .replace(" VOR FRIEDHOF ", " vor Friedhof ")                // Goldemundweg VOR FRIEDHOF(STAND)
        .replace(/ FRIEDHOF KAGRAN$/, " Friedhof Kagran")           // Goldemundweg FRIEDHOF KAGRAN
        .replace(/FRIEDHOF BREITENLEE/, "Friedhof Breitenlee")      // Breitenleer Straße 231 (FRIEDHOF BREITENLEE)
        .replace(" SPORTPLATZ ", " Sportplatz ")                    // Weissauweg 15 SPORTPLATZ (EWSC Donau)
        .replace(" SPARGELFELDSTR. ", " Spargelfeldstraße ")        // Rautenweg SPARGELFELDSTR. (Gasdruckregler)
        .replace(" FISCHERHUETTE ", " Fischerhütte ")               // Revier Mühlleiten FISCHERHUETTE 20
        .replace(" 21 21 Rudda", " 21 Rudda")                       // Gewerbeparkstraße 21 21 Rudda
        .replace(" 4 4 (Bank", " 4 (Bank")                          // Donau-City-Straße 4 4 (Bank Austria)
        .replace(/([0-9])\/P /, "$1 ")                              // Schüttauplatz 2/P gegenüber 2
        .replace(" P/R PARK & RIDE ", " Park & Ride ")
        .replace(" FA.", " Firma ")
        .replace("(FA.", " (Firma ")
        .replace(" Firma STURM", " Firma Sturm")
        .replace(" SIGMA-CONSTRUKTA", " Sigma Constructa")
        .replace(" U/J ", " ")
        .replace("Baeckerei", "Bäckerei")
        .replace("STÄDTISCHES STRANDBAD ALTE DONAU", "Städtisches Strandbad Alte Donau")
        .replace(" SCHILFWEG ", " Schilfweg ")
        .replace(" PARKPL.LOBAUBR./", " Parkplatz Lobaubrücke")
        .replace(" BENNDORFG.", " Benndorfgasse")
        .replace(" FORSTHAUS KUEHWOERTH", " Forsthaus Kühwörth")
        .replace(" VORGARTENSTRASSE", " Vorgartenstraße")
        .replace(" SPORTPLATZ-JAHNWIESE", " Sportplatz Jahnwiese")
        .replace(" SPORTPLATZ-SPORTWIESE", " Sportplatz Sportwiese")
        .replace(" STADIONBR.", " Stadionbrücke")
        .replace(" HAUPTALLEE", " Hauptallee")
        .replace(" FUGBACHGASSE", " Fugbachgasse")
        .replace(" ROTUNDENALLEE", " Rotundenallee")
        .replace(" UNT.DONAUSTR.", " Untere Donaustraße ")
        .replace(" ELEKTRO WIGO", " Elektro WIGO")
        .replace(" MARKTAMT", " Marktamt")
        .replace(" SONDERMUELLCONTAINER", " Sondermüllcontainer")
        .replace(" SPORTPLATZ-AUWIESE", " Sportplatz Auwiese")
        .replace(" HACKGUTSCHUPPEN", " Hackgutschuppen")
        .replace(" FLAKTURM", " Flakturm")
        .replace(" BUNKER-LOKAL", " Bunker-Lokal")
        .replace(/HOF(\d+)/, " Hof $1")
        .replace(" HOF ", " Hof ")
        .replace(" AUSSTELLUNGSSTR. ", " Ausstellungsstraße ")
        .replace(" Pav. ", " Pavillon ")
        .replace(" OPERNG.-TREITLSTR.", " Operngasse-Treitlstraße")
        .replace(" USTRABA.HST.+ ", " USTRABA-Haltestelle ")
        .replace(" STATIONSGEBAEUDE", " Stationsgebäude")
        .replace(" Marktamtsgebaeude", " Marktamtsgebäude")
        .replace(" Parz. ", " Parzelle ")
        .replace(/ Tankstelle(\([A-ZÖÄÜ]+\))/, " Tankstelle $1")
        .replace("Herz Jesu Krankenhaus Herz Jesu Krankenhaus", "Herz Jesu Krankenhaus")
        .replace("REINPRECHTSD.STR.", "Reinprechtsdorfer Straße")
        .replace(/^STBHST. /, "")
        .replace(" STATION ", " Station ")
        .replace(" HALTEST.", " Haltestelle ")
        .replace(" MESSEPLATZ ", " Messeplatz ")
        .replace(" BUFFET ", " Buffet ")
        .replace("SCHLICKPL.", "Schlickplatz")
        .replace("WALDMUELLERPK.-", "Waldmüllerpark")
        .replace("LERCHENFELDER STR.", "Lerchenfelder Straße")
        .replace("FAVORITENSTR.", "Favoritenstraße")
        .replace("BURGENLANDG.", "Burgenlandgasse")
        .replace("O.-WILLMANN-G.", "Otto-Willmann-Gasse")
        .replace("FRANZ-KOCI-STR.", "Franz-Koci-Straße")
        .replace("-FONTANASTR.", "- Fontanastraße")
        .replace("St.Anna Kinderspital St.Anna Kinderspital", "St.Anna Kinderspital")
        .replace("DAUMEGASSE", "Daumegasse")
        .replace("RUDOLFSHUEGELG.", "Rudolfshügelgasse")
        .replace("SILBERGASSE", "Silbergasse")
        .replace("TRAISENG.", "Traisengasse")
        .replace("FRANZ JONAS PLATZ", "Franz-Jonas-Platz")
        .replace("GROSSFELDSTRASSE", "Grossfeldstrasse")
        .replace("-BRÜNNER STR.", "- Brünner Straße")
        .replace("GST.", "Grundstück ")
        .replace(/\/PARZ\.$/, "/Parzelle")
        .replace("KROTTENBACHSTR.", "Krottenbachstraße")
        .replace("LI.V.BAHNUNTERF.", "links von Bahnunterführung")
        .replace("SAEKULAR-INSTITUT", "Säkularinstitut")
        .replace("REITER", "Reiter")
        .replace("WEXSTRASSE", "Wexstraße")
        .replace("(BLOCK A/ ", " (Block A/")
        .replace("(BLOCK A/", " (Block A/")
        .replace(/ EXPEDIT$/, " Expedit")
        .replace(" EKAZENT ", " Ekazent ")
        .replace(" WASSNER)", " Wassner)")
        .replace("SCHILFHUETTEN", "Schilfhütten")
        .replace("PILZG.", "Pilzgasse")
        .replace("GRST.NR.", "Grundstück Nummer ")

        // Stiege
        .replace(/([0-9a-zöäüß]) Stg\. /, "$1 Stiege ")
        .replace(/([0-9a-zöäüß]) STG\. /, "$1 Stiege ")

        // Objekt
        .replace(/([0-9a-zöäüß]) OBJ\. ([A-Z\d]+)/,  "$1 Objekt $2")
        .replace(/([0-9a-zöäüß]) Obj\. ([A-Z\d]+)/,  "$1 Objekt $2")
        .replace(/([0-9a-zöäüß]) OBJEKT ([A-Z\d]+)/, "$1 Objekt $2")

        // Sportplatz-Prefix
        .replace(/^SP-([A-Z]+)/, "$1")
        .replace(/ U-BAHNBG\.(\d+)/, " $1")

        // drop multiple spaces
        .replace(/ +/g, " ");

    if (/Station.+ Station$/.test(replacedName)) {
        replacedName = replacedName.replace(/ Station$/, "")
    }

    replacedName = replacedName
        .replace(/ Messe U-Bahn$/, " Messe");

    return replacedName;
};
