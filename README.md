# ogdwien-address-sanitizer

An address sanitizer for the official OGD addresses provided
by the City of Vienna, Austria.

## Usage

```javascript
const {sanitizeAddressName} =
    require("@stadtkatalog/ogdwien-address-sanitizer");

// prints "Albertinaplatz gegenüber der Oper (Stände)"
console.log(
    sanitizeAddressName("Albertinaplatz GEG.D.OPER(STAENDE)")
);
```

## License

GPLv3 – (C) 2018 Philipp Naderer-Puiu
